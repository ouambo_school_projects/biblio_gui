﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblio_Groupe1.Models
{
    class Livre
    {
        public int? ID_LIVRE { get; set; }
        public string? TITRE { get; set; }
        public string? AUTEUR { get; set; }
        public string? TYPE_LIVRE { get; set; }
        public int? NOMBRE_EXEMPLAIRE { get; set; }
    }
}
