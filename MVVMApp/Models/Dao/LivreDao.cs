﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
namespace Biblio_Groupe1.Models
{
    internal class LivreDao : BaseDao
    {
        /// <summary>
        /// Ajouter un nouveau livre dans la BD (CREATE)
        /// </summary>
        /// <param name="Livre"></param>
        public void CreateLivre(Livre Livre)
        {
            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("INSERT INTO Livre (TITRE, AUTEUR, TYPE_LIVRE, NOMBRE_EXEMPLAIRE) " +
                    "VALUES (@TITRE, @AUTEUR, @TYPE_LIVRE, @NOMBRE_EXEMPLAIRE)", conn);
                cmd.Parameters.AddWithValue("@TITRE", Livre.TITRE);
                cmd.Parameters.AddWithValue("@AUTEUR", Livre.AUTEUR);
                cmd.Parameters.AddWithValue("@TYPE_LIVRE", Livre.TYPE_LIVRE);
                cmd.Parameters.AddWithValue("@NOMBRE_EXEMPLAIRE", Livre.NOMBRE_EXEMPLAIRE);
                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Retourner tous les livres de la BD (READ)
        /// </summary>
        /// <returns>Liste de tous les livres de la BD</returns>
        public List<Livre> GetLivres()
        {
            List<Livre> Livres = new List<Livre>();
            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM livre", conn);

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Livres.Add(new Livre()
                        {
                            ID_LIVRE = Convert.ToInt32(reader["ID_LIVRE"]),
                            TITRE = reader["TITRE"].ToString(),
                            AUTEUR = reader["AUTEUR"].ToString(),
                            TYPE_LIVRE = reader["TYPE_LIVRE"].ToString(),
                            NOMBRE_EXEMPLAIRE = Convert.ToInt32(reader["NOMBRE_EXEMPLAIRE"]),
                        });
                    }
                }
            }
            return Livres;
        }

        /// <summary>
        /// Mofifier un livre dans la Base de données à partir de son ID (UPDATE)
        /// </summary>
        /// <param name="ID_LIVRE"></param>
        /// <param name="TITRE"></param>
        /// <param name="AUTEUR"></param>
        /// <param name="TYPE_LIVRE"></param>
        /// <param name="NOMBRE_EXEMPLAIRE"></param>
        public void UpdateLivre(int ID_LIVRE, string TITRE, string AUTEUR, string TYPE_LIVRE, int NOMBRE_EXEMPLAIRE)
        {
            using(MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("UPDATE Livre SET TITRE = @TITRE, AUTEUR = @AUTEUR, " +
                    "TYPE_LIVRE = @TYPE_LIVRE, NOMBRE_EXEMPLAIRE = @NOMBRE_EXEMPLAIRE WHERE ID_LIVRE = @ID_LIVRE", conn);
                cmd.Parameters.AddWithValue("@TITRE", TITRE);
                cmd.Parameters.AddWithValue("@AUTEUR", AUTEUR);
                cmd.Parameters.AddWithValue("@TYPE_LIVRE", TYPE_LIVRE);
                cmd.Parameters.AddWithValue("@NOMBRE_EXEMPLAIRE", NOMBRE_EXEMPLAIRE);
                cmd.Parameters.AddWithValue("@ID_LIVRE", ID_LIVRE);
                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Supprimer un Livre à partir de son ID (DELETE)
        /// </summary>
        /// <param name="id"></param>
        public void DeleteLivre(int id)
        {
            using(MySqlConnection conn = new MySqlConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    MySqlCommand cmd = new MySqlCommand("DELETE FROM Livre WHERE ID_LIVRE = @ID_LIVRE", conn);
                    cmd.Parameters.AddWithValue("@ID_LIVRE", id);
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex) {  }
            }
        }
    }
}
