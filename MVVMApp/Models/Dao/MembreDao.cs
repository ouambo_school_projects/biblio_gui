﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
namespace Biblio_Groupe1.Models
{
    internal class MembreDao : BaseDao
    {
        public Membre getUser(string userName, string passWord)
        {
            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM membre WHERE USERNAME = @username AND PASSWORD_MEMBRE = @password;", conn);
                cmd.Parameters.AddWithValue("@username", userName);
                cmd.Parameters.AddWithValue("@password", passWord);

                MySqlDataReader rdr = cmd.ExecuteReader();

                if (rdr.Read())
                {
                    return new Membre()
                    {
                        NOM = rdr["NOM"].ToString(),
                        PRENOM = rdr["PRENOM"].ToString(),
                        USERNAME = rdr["USERNAME"].ToString(),
                        ADRESSE = rdr["ADRESSE"].ToString(),
                        PASSWORD_MEMBRE = rdr["PASSWORD_MEMBRE"].ToString(),
                    };
                }
                return new Membre()
                {
                    NOM = "a",
                    PRENOM = "a",
                    USERNAME = "a",
                    ADRESSE = "a",
                    PASSWORD_MEMBRE = "a",
                };
            }
        }
    }
}
