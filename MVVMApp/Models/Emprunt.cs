﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblio_Groupe1.Models
{
    class Emprunt
    {
        public int? ID_LIVRE { get; set; }
        public int? ID_MEMBRE { get; set; }
    }
}
