﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblio_Groupe1.Models
{
    class Membre
    {
        public string? NOM { get; set; }
        public string? PRENOM { get; set; }
        public string? USERNAME { get; set; }
        public string? ADRESSE { get; set; }
        public string? PASSWORD_MEMBRE { get; set; }
    }
}
