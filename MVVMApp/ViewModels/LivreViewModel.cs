﻿using Biblio_Groupe1.Models;
using System.Collections.ObjectModel;
using System.Windows;

namespace Biblio_Groupe1.ViewModels
{
    class LivreViewModel : ViewModelBase
    {
        private ObservableCollection<Livre> livres;

        LivreDao dataAccess = new LivreDao();
        public ObservableCollection<Livre> Livres
        {
            get { return livres; }
            set
            {
                Set(ref livres, value);
            }
        }
        public LivreViewModel()
        {
            livres = new ObservableCollection<Livre>(dataAccess.GetLivres());
        }

        public void CreateLivre(Livre Livre)
        {
            dataAccess.CreateLivre(Livre);
            Application.Current.Dispatcher.Invoke(() => { livres.Add(Livre); });
            RefreshLivres();
        }

        private void Loadlivres()
        {
            var livresFromDb = dataAccess.GetLivres();
            Livres = new ObservableCollection<Livre>(livresFromDb);
        }

        public void UpdateLivre(int ID_LIVRE, string TITRE, string AUTEUR, string TYPE_LIVRE, int NOMBRE_EXAMPLAIRE)
        {
            dataAccess.UpdateLivre(ID_LIVRE, TITRE, AUTEUR, TYPE_LIVRE, NOMBRE_EXAMPLAIRE);
            RefreshLivres();
        }

        public void DeleteLivre(int ID_LIVRE)
        {
            dataAccess.DeleteLivre(ID_LIVRE);
            RefreshLivres() ;
        }
        public void RefreshLivres()
        {
            Loadlivres();
        }
    }
}
