﻿using Biblio_Groupe1.Models;
using Biblio_Groupe1.ViewModels;
using System.Windows.Controls;
using System;
using System.Collections;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;


namespace Biblio_Groupe1.Views
{
    /// <summary>
    /// Interaction logic for LivreView.xaml
    /// </summary>
    public partial class LivreView : UserControl
    {
        public LivreView()
        {
            InitializeComponent();
        }

        private bool isChampsValides(string TITRE, string AUTEUR, string TYPE_LIVRE, string NOMBRE_EXEMPLAIRE)
        {
            int result;
            if(TITRE != "" && AUTEUR != "" && TYPE_LIVRE != "" && NOMBRE_EXEMPLAIRE != "" &&
                    Int32.TryParse(NOMBRE_EXEMPLAIRE, out result) && Int32.Parse(NOMBRE_EXEMPLAIRE) > 0)
                return true;
            infos.Text = "Tous les champs doivent êtres remplis." +
                            " De plus, le nombre d'exemplaire \ndoit être un entier " +
                             "strictement positif.";
            infos.Foreground = Brushes.Red;
            return false;
        }

        private void AfficherAddForm(object sender, RoutedEventArgs e)
        {
            UpdateForm.Visibility = Visibility.Hidden;
            AddForm.Visibility = Visibility.Visible;
        }

        private void SaveLivre(object sender, RoutedEventArgs e)
        {
            if (!isChampsValides(TITRE.Text.ToString(), AUTEUR.Text.ToString(),
                                TYPE_LIVRE.Text.ToString(), NOMBRE_EXEMPLAIRE.Text.ToString()))
                return;

            Livre livre = new Livre { TITRE = TITRE.Text, AUTEUR=AUTEUR.Text, TYPE_LIVRE=TYPE_LIVRE.Text,
                NOMBRE_EXEMPLAIRE = Int32.Parse(NOMBRE_EXEMPLAIRE.Text) };
            var viewModel = DataContext as LivreViewModel;
            viewModel?.CreateLivre(livre);
            TITRE.Text = "";
            NOMBRE_EXEMPLAIRE.Text = "";
            AUTEUR.Text = "";
            NOMBRE_EXEMPLAIRE.Text = "";
            TYPE_LIVRE.Text = "";

            infos.Text = "Ajout effectué avec succès.";
            infos.Foreground = Brushes.Green;
        }

        private void UpdateLivre(object sender, RoutedEventArgs e)
        {
            if (!isChampsValides(TITRE2.Text.ToString(), AUTEUR2.Text.ToString(),
                              TYPE_LIVRE2.Text.ToString(), NOMBRE_EXEMPLAIRE2.Text.ToString()))
            { return; }
               

            var viewModel = DataContext as LivreViewModel;
            viewModel?.UpdateLivre(Int32.Parse(ID2.Text.ToString()), TITRE2.Text.ToString(), 
                                    AUTEUR2.Text.ToString(), TYPE_LIVRE2.Text.ToString(), 
                                    Int32.Parse(NOMBRE_EXEMPLAIRE2.Text.ToString()));

            UpdateForm.Visibility = Visibility.Hidden;
            AddForm.Visibility = Visibility.Visible;

              infos.Text = "Modification effectuée avec succès.";
              infos.Foreground = Brushes.Green;
        }

        private void RefreshLivres(object sender, RoutedEventArgs e)
        {
            LivreViewModel dataAccess = new LivreViewModel();
            dataAccess.RefreshLivres();

        }

        private void SupprimerLivre(object sender, RoutedEventArgs e)
        {
            dynamic livresSelectionnes = ListeLivres.SelectedItems;
            if (livresSelectionnes.Count == 0) 
            {
                infos.Text = "Aucun Livre n'est sélectionné.";
                infos.Foreground = Brushes.Red;
                return; 
            }

            var result = MessageBox.Show($"Voulez - vous supprimer {livresSelectionnes.Count} livre(s) ? ", 
                "Suppression de livres", MessageBoxButton.YesNo);

            if (result == MessageBoxResult.Yes)
            {
                int ID_LIVRE;
                var viewModel = DataContext as LivreViewModel;
                dynamic itemsList =new ArrayList(livresSelectionnes);
                foreach ( var item in itemsList )
                {
                    ID_LIVRE = Int32.Parse((item.ID_LIVRE).ToString());
                    viewModel?.DeleteLivre(ID_LIVRE);
                }

                infos.Text = "Suppression effectuée avec succès.";
                infos.Foreground = Brushes.Green;
            }
        }

        private void AfficherLivre(object sender, RoutedEventArgs e)
        {
            dynamic livresSelectionnes = ListeLivres.SelectedItems;
            if(livresSelectionnes.Count == 0) 
            {
                infos.Text = "Aucun Livre n'est sélectionné.";
                infos.Foreground = Brushes.Red;
                return; 
            }

            AddForm.Visibility = Visibility.Hidden;
            UpdateForm.Visibility = Visibility.Visible;

            dynamic livreSelectionne = livresSelectionnes[0];
            ID2.Text = (livreSelectionne.ID_LIVRE).ToString();
            ID2.IsReadOnly = true;
            TITRE2.Text = (livreSelectionne.TITRE).ToString();
            AUTEUR2.Text = (livreSelectionne.AUTEUR).ToString();
            TYPE_LIVRE2.Text = (livreSelectionne.TYPE_LIVRE).ToString();
            NOMBRE_EXEMPLAIRE2.Text = (livreSelectionne.NOMBRE_EXEMPLAIRE).ToString();
        }

        private void ObjetCLic(object sender, RoutedEventArgs e, dynamic obj)
        {
            obj.Focus();
        }

        private void ObjetChange(object sender, RoutedEventArgs e, dynamic obj, dynamic objHint)
        {
            if (!string.IsNullOrEmpty(obj.Text) && obj.Text.Length > 0)
            {
                objHint.Visibility = Visibility.Collapsed;
            }
            else { objHint.Visibility = Visibility.Visible; }
        }

        private void TitreClic(object sender, MouseButtonEventArgs e)
        {
            ObjetCLic(sender, e, TITRE);
        }

        private void TitreChange(object sender, RoutedEventArgs e)
        {
            ObjetChange(sender, e, TITRE, TITREHINT);
        }       
        private void AuteurClic(object sender, MouseButtonEventArgs e)
        {
            ObjetCLic(sender, e, AUTEUR);
        }

        private void AuteurChange(object sender, RoutedEventArgs e)
        {
            ObjetChange(sender, e, AUTEUR, AUTEUREHINT);
        }

        private void TypeClic(object sender, MouseButtonEventArgs e)
        {
            ObjetCLic(sender, e, TYPE_LIVRE);
        }

        private void TypeChange(object sender, RoutedEventArgs e)
        {
            ObjetChange(sender, e, TYPE_LIVRE, TYPE_LIVREHINT);
        }

        private void NbExemplaireClic(object sender, MouseButtonEventArgs e)
        {
            ObjetCLic(sender, e, NOMBRE_EXEMPLAIRE);
        }

        private void NbExemplaireChange(object sender, RoutedEventArgs e)
        {
            ObjetChange(sender, e, NOMBRE_EXEMPLAIRE, NOMBRE_EXEMPLAIREEHINT);
        }

        private void TitreClic2(object sender, MouseButtonEventArgs e)
        {
            ObjetCLic(sender, e, TITRE2);
        }

        private void TitreChange2(object sender, RoutedEventArgs e)
        {
            ObjetChange(sender, e, TITRE2, TITREHINT2);
        }       
        private void AuteurClic2(object sender, MouseButtonEventArgs e)
        {
            ObjetCLic(sender, e, AUTEUR2);
        }

        private void AuteurChange2(object sender, RoutedEventArgs e)
        {
            ObjetChange(sender, e, AUTEUR2, AUTEUREHINT2);
        }

        private void TypeClic2(object sender, MouseButtonEventArgs e)
        {
            ObjetCLic(sender, e, TYPE_LIVRE2);
        }

        private void TypeChange2(object sender, RoutedEventArgs e)
        {
            ObjetChange(sender, e, TYPE_LIVRE2, TYPE_LIVREHINT2);
        }

        private void NbExemplaireClic2(object sender, MouseButtonEventArgs e)
        {
            ObjetCLic(sender, e, NOMBRE_EXEMPLAIRE2);
        }

        private void NbExemplaireChange2(object sender, RoutedEventArgs e)
        {
            ObjetChange(sender, e, NOMBRE_EXEMPLAIRE2, NOMBRE_EXEMPLAIREEHINT2);
        }
    }
}
