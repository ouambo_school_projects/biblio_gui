﻿using MySql.Data.MySqlClient;
using System.Configuration;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Biblio_Groupe1.Models;
using Biblio_Groupe1.ViewModels;

namespace Biblio_Groupe1.Views
{
    public partial class MainWindow : Window
    {
        MySqlConnection conn = new
        MySqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ConnectionString);
        MySqlCommand cmd;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Send(object sender, RoutedEventArgs e)
        {

            //WelcomeWindows second = new WelcomeWindows();
            this.Close();
            //second.Show();
        }

        private void textUserNameChange(object sender, TextChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(userName.Text) && userName.Text.Length > 0)
            {
                userNameHint.Visibility = Visibility.Collapsed;
            }
            else { userNameHint.Visibility = Visibility.Visible; }
        }

        private void textUserNameClic(object sender, MouseButtonEventArgs e)
        {
            userName.Focus();
        }

        private void textPasswordClic(object sender, MouseButtonEventArgs e)
        {
            password.Focus();

        }

        private void textPasswordChange(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(password.Password) && password.Password.Length > 0)
            {
                passwordHint.Visibility = Visibility.Collapsed;
            }
            else { passwordHint.Visibility = Visibility.Visible; }
        }

        private bool isUserValid(string userName, string passWord)
        {
            conn.Open();
            MySqlCommand cmd = new MySqlCommand("SELECT * FROM membre WHERE USERNAME = @username AND PASSWORD_MEMBRE = @password;", conn);
            cmd.Parameters.AddWithValue("username", userName);
            cmd.Parameters.AddWithValue("password", passWord);
            MySqlDataReader rdr = cmd.ExecuteReader();
            return rdr.Read();
        }

        private void Connexion(object sender, RoutedEventArgs e)
        {
            var viewModel = DataContext as MembreViewModel;

            if (string.IsNullOrEmpty(userName.Text) || string.IsNullOrEmpty(password.Password))
            {
                MessageBox.Show("Veuillez remplir tous les champs", "Données incomplètes");
                return;
            }
            try
            {
                if (isUserValid(userName.Text.ToString(), password.Password.ToString()))
                {
                    WelcomeWindows welcomeWindows = new WelcomeWindows();
                    this.Close();
                    welcomeWindows.Show();
                    return;
                }
                MessageBox.Show("Mot de passe ou nom utilisateur incorrect", "Données incorrectes");
            }
            catch (MySqlException ex) { MessageBox.Show(ex.ToString()); }
            finally { conn.Close(); }
        }

        /// <summary>
        /// Pour Deplacer la fenetre
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeplacerFenetre(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                this.DragMove();
            }
        }

        /// <summary>
        /// Pour fermer l'application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FermerApp(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Voulez-vous vraiment quitter l'application ?", "Quitter BIBLIO SOFT", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                this.Close();
            }
        }
    }
}

