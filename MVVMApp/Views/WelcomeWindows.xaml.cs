﻿using MySql.Data.MySqlClient;
using System.Configuration;
using System.Windows;

namespace Biblio_Groupe1.Views
{
    public partial class WelcomeWindows : Window
    {
        MySqlConnection conn = new
        MySqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ConnectionString);
        public WelcomeWindows()
        {
            InitializeComponent();
            EspaceDeTravail.Content = new Control_Accueil();
        }

        /// <summary>
        /// Gestion du clic sur le menu Accueil
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Accueil(object sender, RoutedEventArgs e)
        {
            //On modifie le titre de la fenetre
            Title = "Accueil";

            //On fait appel au User Control qui gere l'accueil
            EspaceDeTravail.Content = new Control_Accueil();
        }

        /// <summary>
        /// Gestion du clic sur le menu Livres
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Livres(object sender, RoutedEventArgs e)
        {
            //On modifie le titre de la fenetre
            Title = "Livres";

            //On fait appel au User Control qui gere les Livres
            EspaceDeTravail.Content = new LivreView();
        }

        /// <summary>
        /// Gestion du clic sur le menu Membres
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Membres(object sender, RoutedEventArgs e)
        {
            //On modifie le titre de la fenetre
            Title = "Membres";
        }

        /// <summary>
        /// Gestion du clic sur le menu Emprunts
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Emprunts(object sender, RoutedEventArgs e)
        {
            //On modifie le titre de la fenetre
            Title = "Emprunts";
        }

        /// <summary>
        /// Gestion du clic sur le menu A Popoos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void APropos(object sender, RoutedEventArgs e)
        {
            //On modifie le titre de la fenetre
            Title = "A Propos";
        }

        private void Deconnecter(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Voulez-vous vraiment quitter l'application ?", "Quitter DAM SOFT", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                this.Close();
            }
        }
    }
}
